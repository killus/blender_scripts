"""

RenderNormalmap.py

Renders the scene with normal pass and saves the result as NumPy array. The components of the
normal vectors are stored in the RGB color channels. The x and y components are in range [-1, 1].
Since the values can be negative they probably have to be transformed before being displayed as
image.

"""

OUTPUT_FILE = 'normalmap.npy'


import bpy
import numpy as np


# Backup current settings
prev_render_engine = bpy.context.scene.render.engine
prev_use_pass_normal = bpy.context.scene.render.layers[0].use_pass_normal
prev_use_nodes = bpy.context.scene.use_nodes = True
prev_cycles_samples = bpy.context.scene.cycles.samples

# Setup render engine and render layers.
# This implementation only uses the first render layer even if the scene has multiple render layers.
# Only one sample is used per pixel to increase render speed since the normal pass does not change
# with multiple samples anyway.
bpy.context.scene.render.engine = 'CYCLES'
bpy.context.scene.render.layers[0].use_pass_normal = True
bpy.context.scene.cycles.samples = 1

# Enable the compositing node tree
bpy.context.scene.use_nodes = True
tree = bpy.context.scene.node_tree

# Create nodes (the viewer node is needed as workaround to access individual pixels)
rl = tree.nodes.new('CompositorNodeRLayers')
v = tree.nodes.new('CompositorNodeViewer')

# Link output of normal pass to input of viewer
tree.links.new(rl.outputs['Normal'], v.inputs['Image'])

# Render and copy result into numpy array. Only the RGB components are copied while the alpha
# channel is omited. In addition the obtained normal map is flipped vertically.
bpy.ops.render.render()
v.select = True
normal_img = bpy.data.images['Viewer Node']
width, height = normal_img.size
normals = np.array(normal_img.pixels).reshape(height, width, -1)[..., 0:3]
normals = np.flipud(normals)
normals[...,0] *= -1

# Save to disk
np.save(OUTPUT_FILE, normals)

# Clean up
tree.nodes.remove(rl)
tree.nodes.remove(v)

# Restore previous settings
bpy.context.scene.render.engine = prev_render_engine
bpy.context.scene.render.layers[0].use_pass_normal = prev_use_pass_normal
bpy.context.scene.use_nodes = prev_use_nodes
bpy.context.scene.cycles.samples = prev_cycles_samples

print("OK, script has finished")
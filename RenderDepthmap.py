"""

RenderDepthmap.py

Renders the scene with z-pass (i.e. depth map) and saves the result as NumPy array. The output
values are in blender units regardless of the unit settings of the scene.

By default the z-pass in Blender contains the actual length of each view ray into the scene.
This script additionally transforms the obtained z-pass such that only the component parallel
to the optical axis of the camera is returned (e.g. a plane parallel to the image plane
gives uniform depth values).

CAUTION: The depth map created by the Cycles render engine is not error-free.
         A test with a plane at 1 meter distance and 16mm focal length exposed errors in the order
         of 10^-6 meters.
         
"""

OUTPUT_FILE = 'depthmap.npy'


import bpy
import numpy as np


# Backup current settings
prev_render_engine = bpy.context.scene.render.engine
prev_use_pass_z = bpy.context.scene.render.layers[0].use_pass_z
prev_use_nodes = bpy.context.scene.use_nodes
prev_cycles_samples = bpy.context.scene.cycles.samples

# Setup render engine and render layers
# This implementation only uses the first render layer even if the scene has multiple render layers.
# Only one sample is used per pixel to increase render speed since the result of the z-pass does not
# change with multiple samples anyway.
bpy.context.scene.render.engine = 'CYCLES'
bpy.context.scene.render.layers[0].use_pass_z = True
bpy.context.scene.cycles.samples = 1

# Enable the compositing node tree
bpy.context.scene.use_nodes = True
tree = bpy.context.scene.node_tree
links = tree.links

# Create nodes (the viewer node is needed as workaround to access individual pixels)
rl = tree.nodes.new('CompositorNodeRLayers')
v = tree.nodes.new('CompositorNodeViewer')

# Link output of z-pass to input of viewer
tree.links.new(rl.outputs['Z'], v.inputs['Image'])


# Render and copy result into numpy array:
#   In Blender the image of the selected viewer node is available as 'Viewer Node' image.
#   The result contains 4 channels (RGBA). Since each channel provides the same depth values
#   the first channel is simply used here.
#   In addition the obtained depth map is flipped vertically.
bpy.ops.render.render()

v.select = True
depth_img = bpy.data.images['Viewer Node']

width, height = depth_img.size
depth_values = np.copy(np.array(depth_img.pixels).reshape(height, width, -1)[..., 0])
depth_values = np.flipud(depth_values)


# Project depth values onto optical axis:
#
#           ----------
#           |       /
#        Z  |      /
#           |     /  D
#           | x  /
#      ------------------ image plane
#           |  /
#        f  | /  d
#           |/
#           O
#
# D : Depth values returned by the cycles z-pass
# f : Focal length
# x : Pixel position on the image plane
#
# The sought depth value Z can be computed as follows:
# 
# d = sqrt(x^2 + y^2 + f^2)
# Z = f * D / d

# Sensor dimensions and pixel size
sensor_width = bpy.context.scene.camera.data.sensor_width / 1000.0
sensor_height = sensor_width * (height / width)
px_size = sensor_width / width

# Create grid with real world positions for each pixel
img_coord = ( np.indices(depth_values.shape).astype(np.float32) + 0.5 ) * px_size
img_coord[0] -= sensor_height / 2.0
img_coord[1] -= sensor_width / 2.0

# Extend the 2D pixel position grid in 3rd dimension. The camera center is in the origin.
# All pixels lie on the plane at z = focal length.
f = bpy.context.scene.camera.data.lens / 1000.0
img_coord = np.concatenate((img_coord, np.ones(depth_values.shape)[np.newaxis, ...] * f))

# Calculate transformed depth values
z_values = f * depth_values / np.linalg.norm(img_coord, axis=0)


# Save to disk
np.save(OUTPUT_FILE, z_values)

# Clean up
tree.nodes.remove(rl)
tree.nodes.remove(v)

# Restore previous settings
bpy.context.scene.render.engine = prev_render_engine
bpy.context.scene.render.layers[0].use_pass_z = prev_use_pass_z
bpy.context.scene.use_nodes = prev_use_nodes
bpy.context.scene.cycles.samples = prev_cycles_samples

print("OK, script has finished!")